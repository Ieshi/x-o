import { trigger, state, style, transition, animate, query } from "@angular/animations";


export let animateSquare = trigger('animateSquare', [
    transition('none <=> squareAnim',[
        animate(500)
      ]),
      //state('squareAnim', style({transform: 'translateX(10%)'})),
      //state('none', style({transform: 'translateX(0%)'})),
  ]);

export let animateLine = trigger('animateLine', [
    transition('none <=> showHorizontalLine',[
           animate('700ms ease-in-out')
    ]),
    transition('none <=> showVerticalLine',[
        animate(0)
    ]),
    transition('none <=> showHorizontalLine',[
        animate(0)
    ]),
    transition('none <=> showHorizontalLine',[
        animate(0)
    ]),
    state('showHorizontalLine', style({visibility: 'visible'})),
    state('showVerticalLine', style({visibility: 'visible'})),
    state('showLeftDiagLine', style({visibility: 'visible'})),
    state('showRightDiagLine', style({visibility: 'visible'})),
    state('none', style({visibility: 'hidden'})),
]
)
  
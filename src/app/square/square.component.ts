import { ChangeDetectionStrategy, Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { animateLine, animateSquare } from './square-animations';

@Component({
  selector: 'app-square',
  templateUrl: './square.component.html',
  styleUrls: ['./square.component.scss'],
  animations: [animateSquare, animateLine]
})
export class SquareComponent {
  @Input() index : any;
  @Input() value! : 'X' | 'O' | '';
  @Input() winningLine : any;

  animationState = 'none';

  showHorizontalLine = 'none';
  showVerticalLine = 'none';
  showLeftDiagLine = 'none';
  showRightDiagLine = 'none';



  ngOnChanges(changes: SimpleChanges): void {
    if(this.winningLine[0] == this.index || this.winningLine[1] == this.index || this.winningLine[2] == this.index){
      this.animationState = 'squareAnim';

      if(this.winningLine.every((elem: number) => [0, 1, 2].includes(elem)) 
        || this.winningLine.every((elem: number) => [3, 4, 5].includes(elem)) 
        || this.winningLine.every((elem: number) => [6, 7, 8].includes(elem)) 
      ){this.showHorizontalLine = 'showHorizontalLine';}

      if(this.winningLine.every((elem: number) => [0, 3, 6].includes(elem)) 
        || this.winningLine.every((elem: number) => [1, 4, 7].includes(elem)) 
        || this.winningLine.every((elem: number) => [2, 5, 8].includes(elem)) 
      ){this.showVerticalLine = 'showVerticalLine';}

      if(this.winningLine.every((elem: number) => [0, 4, 8].includes(elem))) {this.showLeftDiagLine = 'showLeftDiagLine';}
      
      if(this.winningLine.every((elem: number) => [2, 4, 6].includes(elem))) {this.showRightDiagLine = 'showRightDiagLine';}
    }
  }

}

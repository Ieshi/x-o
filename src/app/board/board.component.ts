import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss'],
})
export class BoardComponent implements OnInit {
  squareArray : any[];
  xIsNext : boolean;
  winner : string;
  effective_winning_line : any[];

  @Output() eventEmitter = new EventEmitter<string>();

  WINNING_LINES = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
  ];


  constructor(){
    this.squareArray = Array(9).fill(null);
    this.winner = "";
    this.xIsNext = true;
    this.effective_winning_line = Array(9).fill(null);
  }


  ngOnInit(){
    this.newGame();
  }

  newGame(){
    this.squareArray = Array(9).fill(null);
    this.winner = "";
    this.xIsNext = true;
    this.effective_winning_line = Array(9).fill(null);
  }

  get player(){
    return this.xIsNext ? 'X' : 'O';
  }

  makeMove(idx: number){


    if(!this.squareArray[idx]){
      this.squareArray.splice(idx, 1, this.player);
      this.xIsNext = !this.xIsNext;
    }
    
    this.winner = this.calculateWinner();

    if(this.winner != null){
      this.sendElementToParent(this.winner)

      this.openModal();
      setTimeout(function() {
        const modalDiv = document.getElementById('winnerModal');
        if(modalDiv != null){
          modalDiv.style.display = 'none';
        } 
      }, 4000);
      
      setTimeout(() => {
        this.newGame();
     }, 4000);

    }

  }

  calculateWinner(){
    for(let i = 0; i<this.WINNING_LINES.length; i++){
      const [a, b, c] = this.WINNING_LINES[i];
      if(
        this.squareArray[a] && this.squareArray[a] === this.squareArray[b] && this.squareArray[a] === this.squareArray[c]
      ){
        this.effective_winning_line = [a, b, c];
        return this.squareArray[a];
      }
    }

    return null;
  }

  openModal(){
    const modalDiv = document.getElementById('winnerModal');
    if(modalDiv != null){
      modalDiv.style.display = 'block';
    } 
  }

  getEffectiveWinningLine(){
    return this.effective_winning_line;
  }

  sendElementToParent(value: string) {
    this.eventEmitter.emit(value);
  }

}

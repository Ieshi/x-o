import { trigger, state, style, transition, animate } from "@angular/animations";


export let moveHorizontal = trigger('moveHorizontal', [

  transition('start <=> end',[
    animate(8000)
  ]),
  state('start', style({transform: 'translateX(80%)'})),
  state('end', style({transform: 'translateX(0%)'}))

])


export let showWinningBanner = trigger('showWinningBanner', [

  transition('* => X',[
    animate(1000)
  ]),
  state('X', style({'color': '#00d68f'})),

  transition('* => O',[
    animate(1000)
  ]),
  state('O', style({'color': '#42aaff'})),

])

export let startFireworks = trigger('startFireworks', [

  transition('* => start',[
    animate(4000)
  ]),
  transition('* => stop',[
    animate(4000)
  ]),
  state('start', style({
    width: '100%',
    height: '100%',
    'z-index': 2
  })),
  state('stop', style({
    width: '0%',
    height: '0%',
    'z-index': 0
  }))

])
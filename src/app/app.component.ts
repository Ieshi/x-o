import { Component, Input } from '@angular/core';
import { moveHorizontal, showWinningBanner, startFireworks } from './animations';

import { Fireworks } from 'fireworks-js'


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    moveHorizontal,
    showWinningBanner,
    startFireworks
  ]
})
export class AppComponent {
  title = 'x-o';
  state: string = 'start';

  @Input() winner: any;

  fireworks: any;

  fireworksContainer : any;



  
  constructor(){
    this.winner = null;
    this.state = 'start';
  }

    move() {
      this.state = (this.state == 'start' ? 'end' : 'start');
    }

    displayWinnerInBanner(winner : string){
      this.winner = winner;
      this.lauchFireworks();

      setTimeout(()=>{
        this.winner = null;
        this.state = 'start';

        if(this.fireworks !=null){
          this.fireworks.stop(true);
          if(this.fireworksContainer != null)
            this.fireworksContainer.style.zIndex = '0';
        }
        
      }, 4000);
    }

    lauchFireworks(){
      this.fireworksContainer = document.getElementById('fireWorksContainer');

      if(this.fireworksContainer != null){
        this.fireworks = new Fireworks(this.fireworksContainer);
      }

      if(this.fireworks !=null){
        this.fireworks.start(true);
        if(this.fireworksContainer != null)
          this.fireworksContainer.style.zIndex = '2';
      }
    }

}
